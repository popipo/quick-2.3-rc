﻿/*
 * cocos2d for iPhone: http://www.cocos2d-iphone.org
 *
 * Copyright (c) 2011 Ricardo Quesada
 * Copyright (c) 2012 Zynga Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
  */

//"													\n\
//attribute vec4 a_position;							\n\
//uniform	vec4 u_color;								\n\
//uniform float u_pointSize;							\n\
//													\n\
//#ifdef GL_ES										\n\
//varying lowp vec4 v_fragmentColor;					\n\
//#else												\n\
//varying vec4 v_fragmentColor;						\n\
//#endif												\n\
//													\n\
//void main()											\n\
//{													\n\
//    gl_Position = CC_MVPMatrix * a_position;		\n\
//	gl_PointSize = u_pointSize;						\n\
//	v_fragmentColor = u_color;						\n\
//}													\n\
//";

//直线 Ax＋By＋C＝0
//d=[Ax0+By0+C的绝对值]/[(A^2+B^2)的算术平方根]

//"													\n\
//attribute vec4 a_position;							\n\
//uniform	vec4 u_color;								\n\
//uniform float u_pointSize;							\n\
//uniform  vec3  u_end;  \n\
//uniform  float  u_size;  \n\
//#ifdef GL_ES										\n\
//varying lowp vec4 v_fragmentColor;					\n\
//#else												\n\
//varying vec4 v_fragmentColor;						\n\
//#endif												\n\
//\n\
//void main()											\n\
//{													\n\
//gl_Position = CC_MVPMatrix * a_position;		\n\
//gl_PointSize = u_pointSize;						\n\
//\n\
//\n\
//if(a_position[1] >  ((u_end[0]*a_position[0]+u_end[2])*-1.0) / u_end[1] ){\n\
//\n\
//v_fragmentColor = vec4(u_color[0], u_color[1],u_color[2],u_color[3]*abs((abs(u_end[0]*a_position[0]+u_end[1]*a_position[1]+u_end[2])/sqrt(u_end[0]*u_end[0]+u_end[1]*u_end[1])/u_size)));	\n\
//}else{\n\
//    \n\
//    v_fragmentColor = vec4(u_color[0], u_color[1],u_color[2],u_color[3]*abs(1.0-(abs(u_end[0]*a_position[0]+u_end[1]*a_position[1]+u_end[2])/sqrt(u_end[0]*u_end[0]+u_end[1]*u_end[1])/u_size)));\n\
//}\n\
//}\n\
//";

// "													\n\
// attribute vec4 a_position;							\n\
// uniform	vec4 u_color;								\n\
// uniform float u_pointSize;							\n\
// uniform  vec3  u_end;  \n\
// uniform  float  u_size;  \n\
// #ifdef GL_ES										\n\
// varying lowp vec4 v_fragmentColor;					\n\
// #else												\n\
// varying vec4 v_fragmentColor;						\n\
// #endif												\n\
// \n\
// void main()											\n\
// {													\n\
// gl_Position = CC_MVPMatrix * a_position;		\n\
// gl_PointSize = u_pointSize;						\n\
// \n\
// v_fragmentColor = vec4(u_color[0], u_color[1],u_color[2],u_color[3]*abs(1.0-(abs(u_end[0]*a_position[0]+u_end[1]*a_position[1]+u_end[2])/sqrt(u_end[0]*u_end[0]+u_end[1]*u_end[1])/u_size)));	\n\
// }\n\
// ";

// "													\n\
// attribute vec4 a_position;							\n\
// attribute vec4 a_Rect;							\n\
// uniform	vec4 u_color;								\n\
// uniform float u_pointSize;							\n\
// uniform  vec3  u_end;  \n\
// uniform  float  u_size;  \n\
// uniform  vec4 u_Rect;  \n\
// uniform  int   u_RectUse;  \n\
// #ifdef GL_ES										\n\
// varying lowp vec4 v_fragmentColor;					\n\
// #else												\n\
// varying vec4 v_fragmentColor;						\n\
// #endif												\n\
// \n\
// void main()											\n\
// {													\n\
// gl_Position = CC_MVPMatrix * a_position;		\n\
// gl_PointSize = u_pointSize;						\n\
// \n\
// v_fragmentColor = vec4(u_color[0], u_color[1],u_color[2],u_color[3]*abs(1.0-(abs(u_end[0]*a_position[0]+u_end[1]*a_position[1]+u_end[2])/sqrt(u_end[0]*u_end[0]+u_end[1]*u_end[1])/u_size)));\n\
// }\n\
// ";

"													\n\
attribute vec4 a_position;							\n\
attribute vec4 a_color;							\n\
uniform float u_pointSize;							\n\
#ifdef GL_ES										\n\
varying lowp vec4 v_fragmentColor;					\n\
#else												\n\
varying vec4 v_fragmentColor;						\n\
#endif												\n\
\n\
void main()											\n\
{													\n\
gl_Position = CC_MVPMatrix * a_position;		\n\
gl_PointSize = u_pointSize;						\n\
\n\
v_fragmentColor = a_color;\n\
}";


