//
//  XSDrawNode.h
//  quickcocos2dx
//
//  Created by mac on 15/5/6.
//  Copyright (c) 2015年 qeeplay.com. All rights reserved.
//

#ifndef __quickcocos2dx__XSDrawNode__
#define __quickcocos2dx__XSDrawNode__

#include <stdio.h>
#include "cocos2d.h"

NS_CC_BEGIN

class XSDrawNode : public cocos2d::CCNode{
    
    CCGLProgram* _p;
    
    ccVertex2F vertices3[8];
    ccColor4F  colors3[8];
    
    
    int _unSize;
    int _attrPoint;
    int _attrColor;
    
public:
    void drawLine();
    void setPoint(const CCPoint& point, int index);
    void setColor(const ccColor4F& color, int index);
    
    static XSDrawNode* create();
    
    virtual bool  init();
    
    ~XSDrawNode();
};

NS_CC_END

#endif /* defined(__quickcocos2dx__XSDrawNode__) */
