//
//  XSDrawNode.cpp
//  quickcocos2dx
//
//  Created by mac on 15/5/6.
//  Copyright (c) 2015年 qeeplay.com. All rights reserved.
//

#include "XSDrawNode.h"

USING_NS_CC;

XSDrawNode* XSDrawNode::create(){

    XSDrawNode* pRet = new XSDrawNode();
    if (pRet && pRet->init()) {
        pRet->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(pRet);
    }
    
    return pRet;
}

XSDrawNode::~XSDrawNode(){

    delete _p;
    _p = NULL;
}

bool XSDrawNode::init(){

    _p = new CCGLProgram();
    
    const GLchar* frag =
#include "_XSDrawfrag.h"
    const GLchar* vert =
#include "_XSDrawvert.h"
    
    _p->initWithVertexShaderByteArray(vert, frag);
    _p->link();
    _p->updateUniforms();
    
    CHECK_GL_ERROR_DEBUG();
    
    

    _unSize = glGetUniformLocation( _p->getProgram(), "u_pointSize");
    CHECK_GL_ERROR_DEBUG();
    _attrColor = glGetAttribLocation(_p->getProgram(), "a_color");
    CHECK_GL_ERROR_DEBUG();
    _attrPoint = glGetAttribLocation(_p->getProgram(), "a_position");
    CHECK_GL_ERROR_DEBUG();
    
    
    return true;
}


void XSDrawNode::setPoint(const cocos2d::CCPoint &point, int index){

    
    vertices3[index] = vertex2(point.x,point.y);
}

void XSDrawNode::setColor(const ccColor4F &color, int index){
    
    colors3[index] = ccc4f(color.r,color.g,color.b,color.a);
}


void XSDrawNode::drawLine(){

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA);
    
    _p->use();
    _p->setUniformsForBuiltins();
    //_p->setUniformLocationWith4fv(_unSize, (GLfloat*) &s_tColor.r, 1);
    
    

    
    glEnableVertexAttribArray(_attrPoint);
    glVertexAttribPointer(_attrPoint, 2, GL_FLOAT, GL_FALSE, 0, vertices3);
    
    
    glEnableVertexAttribArray(_attrColor);
    glVertexAttribPointer(_attrColor,4,GL_FLOAT,GL_FALSE,0,colors3);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 8);

}















