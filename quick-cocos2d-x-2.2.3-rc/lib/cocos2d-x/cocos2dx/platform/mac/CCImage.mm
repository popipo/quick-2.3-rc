/****************************************************************************
Copyright (c) 2010 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
#include <Foundation/Foundation.h>
#include <Cocoa/Cocoa.h>
#include "CCDirector.h"
#include "ccMacros.h"
#include "CCImage.h"
#include "CCFileUtils.h"
#include "CCTexture2D.h"
#include <string>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include "apptools/HelperFunc.h"

typedef struct
{
    unsigned int height;
    unsigned int width;
    int         bitsPerComponent;
    bool        hasAlpha;
    bool        isPremultipliedAlpha;
    unsigned char*  data;
} tImageInfo;

static unsigned int nextPOT(unsigned int x)
{
    x = x - 1;
    x = x | (x >> 1);
    x = x | (x >> 2);
    x = x | (x >> 4);
    x = x | (x >> 8);
    x = x | (x >> 16);
    return x + 1;
}

typedef enum {
    kCCTexture2DPixelFormat_Automatic = 0,
        //! 32-bit texture: RGBA8888
    kCCTexture2DPixelFormat_RGBA8888,
        //! 24-bit texture: RGBA888
    kCCTexture2DPixelFormat_RGB888,
        //! 16-bit texture without Alpha channel
    kCCTexture2DPixelFormat_RGB565,
        //! 8-bit textures used as masks
    kCCTexture2DPixelFormat_A8,
        //! 16-bit textures: RGBA4444
    kCCTexture2DPixelFormat_RGBA4444,
        //! 16-bit textures: RGB5A1
    kCCTexture2DPixelFormat_RGB5A1,    
    
        //! Default texture format: RGBA8888
    kCCTexture2DPixelFormat_Default = kCCTexture2DPixelFormat_RGBA8888,
    
        // backward compatibility stuff
    kTexture2DPixelFormat_Automatic = kCCTexture2DPixelFormat_Automatic,
    kTexture2DPixelFormat_RGBA8888 = kCCTexture2DPixelFormat_RGBA8888,
    kTexture2DPixelFormat_RGB888 = kCCTexture2DPixelFormat_RGB888,
    kTexture2DPixelFormat_RGB565 = kCCTexture2DPixelFormat_RGB565,
    kTexture2DPixelFormat_A8 = kCCTexture2DPixelFormat_A8,
    kTexture2DPixelFormat_RGBA4444 = kCCTexture2DPixelFormat_RGBA4444,
    kTexture2DPixelFormat_RGB5A1 = kCCTexture2DPixelFormat_RGB5A1,
    kTexture2DPixelFormat_Default = kCCTexture2DPixelFormat_Default
    
} CCTexture2DPixelFormat;

static bool _initPremultipliedATextureWithImage(CGImageRef image, NSUInteger POTWide, NSUInteger POTHigh, tImageInfo *pImageInfo)
{
    NSUInteger            i;
    CGContextRef        context = nil;
    unsigned char*        data = nil;;
    CGColorSpaceRef        colorSpace;
    unsigned char*        tempData;
    unsigned int*        inPixel32;
    unsigned short*        outPixel16;
    bool                hasAlpha;
    CGImageAlphaInfo    info;
    CGSize                imageSize;
    CCTexture2DPixelFormat    pixelFormat;
    
    info = CGImageGetAlphaInfo(image);
    hasAlpha = ((info == kCGImageAlphaPremultipliedLast) || (info == kCGImageAlphaPremultipliedFirst) || (info == kCGImageAlphaLast) || (info == kCGImageAlphaFirst) ? YES : NO);
    
    size_t bpp = CGImageGetBitsPerComponent(image);
    colorSpace = CGImageGetColorSpace(image);
    
    if(colorSpace) 
    {
        if(hasAlpha || bpp >= 8)
        {
            pixelFormat = kCCTexture2DPixelFormat_Default;
        }
        else 
        {
            pixelFormat = kCCTexture2DPixelFormat_RGB565;
        }
    } 
    else  
    {
        // NOTE: No colorspace means a mask image
        pixelFormat = kCCTexture2DPixelFormat_A8;
    }
    
    imageSize.width = CGImageGetWidth(image);
    imageSize.height = CGImageGetHeight(image);
    
    // Create the bitmap graphics context
    
    switch(pixelFormat) 
    {      
        case kCCTexture2DPixelFormat_RGBA8888:
        case kCCTexture2DPixelFormat_RGBA4444:
        case kCCTexture2DPixelFormat_RGB5A1:
            colorSpace = CGColorSpaceCreateDeviceRGB();
            data = new unsigned char[POTHigh * POTWide * 4];
            info = hasAlpha ? kCGImageAlphaPremultipliedLast : kCGImageAlphaNoneSkipLast;
            context = CGBitmapContextCreate(data, POTWide, POTHigh, 8, 4 * POTWide, colorSpace, info | kCGBitmapByteOrder32Big);                
            CGColorSpaceRelease(colorSpace);
            break;
            
        case kCCTexture2DPixelFormat_RGB565:
            colorSpace = CGColorSpaceCreateDeviceRGB();
            data = new unsigned char[POTHigh * POTWide * 4];
            info = kCGImageAlphaNoneSkipLast;
            context = CGBitmapContextCreate(data, POTWide, POTHigh, 8, 4 * POTWide, colorSpace, info | kCGBitmapByteOrder32Big);
            CGColorSpaceRelease(colorSpace);
            break;
        case kCCTexture2DPixelFormat_A8:
            data = new unsigned char[POTHigh * POTWide];
            info = kCGImageAlphaOnly;
            context = CGBitmapContextCreate(data, POTWide, POTHigh, 8, POTWide, NULL, info);
            break;            
        default:
            return false;
    }
    
    CGRect rect;
    rect.size.width = POTWide;
    rect.size.height = POTHigh;
    rect.origin.x = 0;
    rect.origin.y = 0;
    
    CGContextClearRect(context, rect);
    CGContextTranslateCTM(context, 0, 0);
    CGContextDrawImage(context, rect, image);
    
    // Repack the pixel data into the right format
    
    if(pixelFormat == kCCTexture2DPixelFormat_RGB565) 
    {
        //Convert "RRRRRRRRRGGGGGGGGBBBBBBBBAAAAAAAA" to "RRRRRGGGGGGBBBBB"
        tempData = new unsigned char[POTHigh * POTWide * 2];
        inPixel32 = (unsigned int*)data;
        outPixel16 = (unsigned short*)tempData;
        for(i = 0; i < POTWide * POTHigh; ++i, ++inPixel32)
        {
            *outPixel16++ = ((((*inPixel32 >> 0) & 0xFF) >> 3) << 11) | ((((*inPixel32 >> 8) & 0xFF) >> 2) << 5) | ((((*inPixel32 >> 16) & 0xFF) >> 3) << 0);
        }

        delete []data;
        data = tempData;
        
    }
    else if (pixelFormat == kCCTexture2DPixelFormat_RGBA4444) 
    {
        //Convert "RRRRRRRRRGGGGGGGGBBBBBBBBAAAAAAAA" to "RRRRGGGGBBBBAAAA"
        tempData = new unsigned char[POTHigh * POTWide * 2];
        inPixel32 = (unsigned int*)data;
        outPixel16 = (unsigned short*)tempData;
        for(i = 0; i < POTWide * POTHigh; ++i, ++inPixel32)
        {
            *outPixel16++ = 
            ((((*inPixel32 >> 0) & 0xFF) >> 4) << 12) | // R
            ((((*inPixel32 >> 8) & 0xFF) >> 4) << 8) | // G
            ((((*inPixel32 >> 16) & 0xFF) >> 4) << 4) | // B
            ((((*inPixel32 >> 24) & 0xFF) >> 4) << 0); // A
        }       
        
        delete []data;
        data = tempData;
        
    }
    else if (pixelFormat == kCCTexture2DPixelFormat_RGB5A1) 
    {
        //Convert "RRRRRRRRRGGGGGGGGBBBBBBBBAAAAAAAA" to "RRRRRGGGGGBBBBBA"
        tempData = new unsigned char[POTHigh * POTWide * 2];
        inPixel32 = (unsigned int*)data;
        outPixel16 = (unsigned short*)tempData;
        for(i = 0; i < POTWide * POTHigh; ++i, ++inPixel32)
        {
            *outPixel16++ = 
            ((((*inPixel32 >> 0) & 0xFF) >> 3) << 11) | // R
            ((((*inPixel32 >> 8) & 0xFF) >> 3) << 6) | // G
            ((((*inPixel32 >> 16) & 0xFF) >> 3) << 1) | // B
            ((((*inPixel32 >> 24) & 0xFF) >> 7) << 0); // A
        }
                
        delete []data;
        data = tempData;
    }
    
    // should be after calling super init
    pImageInfo->isPremultipliedAlpha = true;
    pImageInfo->hasAlpha = true;
    pImageInfo->bitsPerComponent = (int)bpp;
    pImageInfo->width = (unsigned int)POTWide;
    pImageInfo->height = (unsigned int)POTHigh;
    
    if (pImageInfo->data)
    {
        delete [] pImageInfo->data;
    }
    pImageInfo->data = data;
    
    CGContextRelease(context);
    return true;
}
// TODO: rename _initWithImage, it also makes a draw call.
static bool _initWithImage(CGImageRef CGImage, tImageInfo *pImageinfo, double scaleX, double scaleY)
{
    NSUInteger POTWide, POTHigh;
    
    if(CGImage == NULL) 
    {
        return false;
    }
    
	//if (cocos2d::CCImage::getIsScaleEnabled())
	if( cocos2d::CCDirector::sharedDirector()->getContentScaleFactor() > 1.0f )
	{
		POTWide = CGImageGetWidth(CGImage) * scaleX;
		POTHigh = CGImageGetHeight(CGImage) * scaleY;
	}
	else 
	{
		POTWide = CGImageGetWidth(CGImage);
		POTHigh = CGImageGetHeight(CGImage);
	}

    
    // load and draw image
    return _initPremultipliedATextureWithImage(CGImage, POTWide, POTHigh, pImageinfo);
}

static bool _initWithFile(const char* path, tImageInfo *pImageinfo)
{
    CGImageRef                CGImage;    
    NSImage                    *jpg;
    //NSImage                    *png;
    bool            ret;
    
    // convert jpg to png before loading the texture
    
    //NSString *fullPath = [NSString stringWithUTF8String:path];
	unsigned long fileSize = 0;
	unsigned char* pFileData = cocos2d::CZHelperFunc::getFileData(path, "rb", &fileSize);
    NSData *adata = [[NSData alloc] initWithBytes:pFileData length:fileSize];
	delete []pFileData;
    jpg = [[NSImage alloc] initWithData:adata];
    //jpg = [[NSImage alloc] initWithContentsOfFile: fullPath];
    //png = [[NSImage alloc] initWithData:UIImagePNGRepresentation(jpg)];
    CGImageSourceRef source = CGImageSourceCreateWithData((CFDataRef)[jpg TIFFRepresentation], NULL);
    CGImage = CGImageSourceCreateImageAtIndex(source, 0, NULL);
    
    ret = _initWithImage(CGImage, pImageinfo, 1.0, 1.0);
    
    //[png release];
    [jpg release];
    if (CGImage) CFRelease(CGImage);
    if (source) CFRelease(source);
  
    return ret;
}

// TODO: rename _initWithData, it also makes a draw call.
static bool _initWithData(void * pBuffer, int length, tImageInfo *pImageinfo, double scaleX, double scaleY)
{
    bool ret = false;
    
    if (pBuffer) 
    {
        CGImageRef CGImage;
        NSData *data;
        
        data = [NSData dataWithBytes:pBuffer length:length];
		CGImageSourceRef source = CGImageSourceCreateWithData((CFDataRef)data, NULL);
        CGImage = CGImageSourceCreateImageAtIndex(source, 0, NULL);
        
        ret = _initWithImage(CGImage, pImageinfo, scaleX, scaleY);
        if (CGImage) CFRelease(CGImage);
        if (source) CFRelease(source);
    }
    return ret;
}

static bool _isValidFontName(const char *fontName)
{
    bool ret = false;
#if 0 
    NSString *fontNameNS = [NSString stringWithUTF8String:fontName];
    
    for (NSString *familiName in [NSFont familyNames]) 
    {
        if ([familiName isEqualToString:fontNameNS]) 
        {
            ret = true;
            goto out;
        }
        
        for(NSString *font in [NSFont fontNamesForFamilyName: familiName])
        {
            if ([font isEqualToString: fontNameNS])
            {
                ret = true;
                goto out;
            }
        }
    }
#endif
    out:
    return ret;
}

static bool _initWithString(const char * pText, cocos2d::CCImage::ETextAlign eAlign, const char * pFontName, int nSize, tImageInfo* pInfo, cocos2d::ccColor3B* pStrokeColor)
{
    bool bRet = false;

	CCAssert(pText, "Invalid pText");
	CCAssert(pInfo, "Invalid pInfo");
	
	do {
		NSString * string  = [NSString stringWithUTF8String:pText];
		
		// font
		NSFont *font = [[NSFontManager sharedFontManager]
						 fontWithFamily:[NSString stringWithUTF8String:pFontName]
						traits:NSUnboldFontMask | NSUnitalicFontMask
						 weight:0
						 size:nSize];
		
		if (font == nil) {
			font = [[NSFontManager sharedFontManager]
					fontWithFamily:@"Arial"
					traits:NSUnboldFontMask | NSUnitalicFontMask
					weight:0
					size:nSize];
		}
		CC_BREAK_IF(!font);
		
		// color
		NSColor* foregroundColor;
		if (pStrokeColor) {
			foregroundColor = [NSColor colorWithDeviceRed:pStrokeColor->r/255.0 green:pStrokeColor->g/255.0 blue:pStrokeColor->b/255.0 alpha:1];
		} else {
			foregroundColor = [NSColor whiteColor];
		}
		
		
		// alignment, linebreak
		unsigned uHoriFlag = eAlign & 0x0f;
		unsigned uVertFlag = (eAlign >> 4) & 0x0f;
		NSTextAlignment align = (2 == uHoriFlag) ? NSRightTextAlignment
			: (3 == uHoriFlag) ? NSCenterTextAlignment
			: NSLeftTextAlignment;
		
		NSMutableParagraphStyle *paragraphStyle = [[[NSMutableParagraphStyle alloc] init] autorelease];
		[paragraphStyle setParagraphStyle:[NSParagraphStyle defaultParagraphStyle]];
		[paragraphStyle setLineBreakMode:NSLineBreakByCharWrapping];
		[paragraphStyle setAlignment:align];

		// attribute
		NSDictionary* tokenAttributesDict = [NSDictionary dictionaryWithObjectsAndKeys:
											 foregroundColor,NSForegroundColorAttributeName,
											 font, NSFontAttributeName,
											 paragraphStyle, NSParagraphStyleAttributeName, nil];

		// linebreak
		if (pInfo->width > 0) {
			if ([string sizeWithAttributes:tokenAttributesDict].width > pInfo->width) {
				NSMutableString *lineBreak = [[[NSMutableString alloc] init] autorelease];
				NSUInteger length = [string length];
				NSRange range = NSMakeRange(0, 1);
				NSUInteger width = 0;
				NSUInteger lastBreakLocation = 0;
				for (NSUInteger i = 0; i < length; i++) {
					range.location = i;
					NSString *character = [string substringWithRange:range];
					[lineBreak appendString:character];
					if ([@"!?.,-= " rangeOfString:character].location != NSNotFound) { lastBreakLocation = i; }
					width = [lineBreak sizeWithAttributes:tokenAttributesDict].width;
					if (width > pInfo->width) {
						[lineBreak insertString:@"\r\n" atIndex:(lastBreakLocation > 0) ? lastBreakLocation : [lineBreak length] - 1];
					}
				}
				string = lineBreak;
			}
		}

		NSAttributedString *stringWithAttributes =[[[NSAttributedString alloc] initWithString:string
										 attributes:tokenAttributesDict] autorelease];
				
		NSSize realDimensions = [stringWithAttributes size];
		// Mac crashes if the width or height is 0
		CC_BREAK_IF(realDimensions.width <= 0 || realDimensions.height <= 0);
				
		CGSize dimensions = CGSizeMake(pInfo->width, pInfo->height);
		
	
		if(dimensions.width <= 0 && dimensions.height <= 0) {
			dimensions.width = realDimensions.width;
			dimensions.height = realDimensions.height;
		} else if (dimensions.height <= 0) {
			dimensions.height = realDimensions.height;
		}

        dimensions.width = (int)(dimensions.width / 2) * 2 + 2;
        dimensions.height = (int)(dimensions.height / 2) * 2 + 2;

		NSUInteger POTWide = (NSUInteger)dimensions.width;
		NSUInteger POTHigh = (NSUInteger)(MAX(dimensions.height, realDimensions.height));
		unsigned char*			data;
		//Alignment
			
		CGFloat xPadding = 0;
		switch (align) {
			case NSLeftTextAlignment: xPadding = 0; break;
			case NSCenterTextAlignment: xPadding = (dimensions.width-realDimensions.width)/2.0f; break;
			case NSRightTextAlignment: xPadding = dimensions.width-realDimensions.width; break;
			default: break;
		}

		// 1: TOP
		// 2: BOTTOM
		// 3: CENTER
		CGFloat yPadding = (1 == uVertFlag || realDimensions.height >= dimensions.height) ? (dimensions.height - realDimensions.height)	// align to top
		: (2 == uVertFlag) ? 0																	// align to bottom
		: (dimensions.height - realDimensions.height) / 2.0f;									// align to center
		
		
		NSRect textRect = NSMakeRect(xPadding, POTHigh - dimensions.height + yPadding, realDimensions.width, realDimensions.height);
		//Disable antialias
		
		[[NSGraphicsContext currentContext] setShouldAntialias:NO];	
		
		NSImage *image = [[NSImage alloc] initWithSize:NSMakeSize(POTWide, POTHigh)];
        
		[image lockFocus];
        
        // patch for mac retina display and lableTTF
        [[NSAffineTransform transform] set];
		
		//[stringWithAttributes drawAtPoint:NSMakePoint(xPadding, offsetY)]; // draw at offset position	
		[stringWithAttributes drawInRect:textRect];
		//[stringWithAttributes drawInRect:textRect withAttributes:tokenAttributesDict];
		NSBitmapImageRep *bitmap = [[NSBitmapImageRep alloc] initWithFocusedViewRect:NSMakeRect (0.0f, 0.0f, POTWide, POTHigh)];
		[image unlockFocus];
		
		data = (unsigned char*) [bitmap bitmapData];  //Use the same buffer to improve the performance.
		
		NSUInteger textureSize = POTWide*POTHigh*4;
		
		unsigned char* dataNew = new unsigned char[textureSize];
		if (dataNew) {
			memcpy(dataNew, data, textureSize);
			// output params
			pInfo->width = (unsigned int)POTWide;
			pInfo->height = (unsigned int)POTHigh;
			pInfo->data = dataNew;
			pInfo->hasAlpha = true;
			pInfo->isPremultipliedAlpha = true;
			pInfo->bitsPerComponent = 8;
			bRet = true;
		}
		[bitmap release];
		[image release];
	} while (0);
    return bRet;
}

NS_CC_BEGIN

static bool m_bEnabledScale = true;

bool isFileExists(const char* szFilePath);

bool isFileExists(const char* szFilePath)
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
	//TCHAR dirpath[MAX_PATH];
	//MultiByteToWideChar(936,0,szFilePath,-1,dirpath,sizeof(dirpath));
	DWORD dwFileAttr = GetFileAttributesA(szFilePath);
	if (INVALID_FILE_ATTRIBUTES == dwFileAttr
		|| (dwFileAttr&FILE_ATTRIBUTE_DIRECTORY))	{
		return false;
	}		
#elif CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
	bool bFind = true;
	do 
	{
		struct stat buf;
		int n = stat(szFilePath, &buf);
		if ((0 != n)
			|| !(buf.st_mode&S_IFMT))	
		{
			bFind = false;
		}
	} while (0);
	if (!bFind)
	{
		//std::string strFilenName = s_strRelativePath + szFilePath;
		unsigned char * pBuffer = NULL;
		unzFile pFile = NULL;
		unsigned long pSize = 0;
		
		do 
		{
			pFile = unzOpen(s_strAndroidPackagePath.c_str());
			if(!pFile)break;
			
			int nRet = unzLocateFile(pFile, szFilePath, 1);
			if(UNZ_OK != nRet)
				bFind = false;
			else
				bFind = true;
		} while (0);
		
		if (pFile)
		{
			unzClose(pFile);
		}
	}
	
	return bFind;
#else
	struct stat buf;
	int n = stat(szFilePath, &buf);
	if ((0 != n)
		|| !(buf.st_mode&S_IFMT))	{
		return false;
	}		
	
#endif
	return true;
}

CCImage::CCImage()
: m_nWidth(0)
, m_nHeight(0)
, m_nBitsPerComponent(0)
, m_pData(0)
, m_bHasAlpha(false)
, m_bPreMulti(false)
{
    
}

CCImage::~CCImage()
{
    CC_SAFE_DELETE_ARRAY(m_pData);
}

bool CCImage::initWithImageFile(const char * strPath, EImageFormat eImgFmt/* = eFmtPng*/)
{
    std::string strTemp = CCFileUtils::sharedFileUtils()->fullPathForFilename(strPath);
	if (m_bEnabledScale)
	{
		if (!isFileExists(strTemp.c_str()))
		{
			if (strTemp.rfind("@2x") == std::string::npos)
			{
				size_t t = strTemp.rfind(".");
				if (t != std::string::npos)
				{
					strTemp.insert(t, "@2x");
				}
/*				CCSize size = CCDirector::sharedDirector()->getWinSize();		
	#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
				m_dScaleX = size.width/800.0f;
				m_dScaleY = size.height/480.0f;
	#else
				m_dScaleX = size.width/960.0f;
				m_dScaleY = size.height/640.0f;
				
	#endif
*/
			}
		}    
		else
		{
//			m_dScaleX = 1.0;
//			m_dScaleY = 1.0;
		}
	}
	
//	CCFileData tempData(strTemp.c_str(), "rb");			
//	return initWithImageData(tempData.getBuffer(), tempData.getSize(), eImgFmt);

	unsigned long fileSize = 0;
	//unsigned char* pFileData = CCFileUtils::sharedFileUtils()->getFileData(strTemp.c_str(), "rb", &fileSize);
	unsigned char* pFileData = CZHelperFunc::getFileData(strTemp.c_str(), "rb", &fileSize);
	bool ret = initWithImageData(pFileData, (int)fileSize, eImgFmt);
	delete []pFileData;
	return ret;
}

bool CCImage::initWithImageFileThreadSafe(const char *fullpath, EImageFormat imageType)
{
    /*
     * CCFileUtils::fullPathFromRelativePath() is not thread-safe, it use autorelease().
     */
    bool bRet = false;
    unsigned long nSize = 0;
    //unsigned char* pBuffer = CCFileUtils::sharedFileUtils()->getFileData(fullpath, "rb", &nSize);
    unsigned char* pBuffer = CZHelperFunc::getFileData(fullpath, "rb", &nSize);
    if (pBuffer != NULL && nSize > 0)
    {
        bRet = initWithImageData(pBuffer, (int)nSize, imageType);
    }
    CC_SAFE_DELETE_ARRAY(pBuffer);
    return bRet;
}



/*
// please uncomment this and integrate it somehow if you know what your doing, thanks
bool CCImage::potImageData(unsigned int POTWide, unsigned int POTHigh)
{
	unsigned char*			data = NULL;
	unsigned char*			tempData =NULL;
	unsigned int*				inPixel32 = NULL;
	unsigned short*			outPixel16 = NULL;
	bool					hasAlpha;
	CCTexture2DPixelFormat	pixelFormat;
	
	hasAlpha = this->hasAlpha();
	
	size_t bpp = this->getBitsPerComponent();
	
    // compute pixel format
	if(hasAlpha)
	{
		pixelFormat = CCTexture2D::defaultAlphaPixelFormat();
	}
	else
	{
		if (bpp >= 8)
		{
			pixelFormat = kCCTexture2DPixelFormat_RGB888;
		}
		else
		{
			CCLOG("cocos2d: CCTexture2D: Using RGB565 texture since image has no alpha");
			pixelFormat = kCCTexture2DPixelFormat_RGB565;
		}
	}
	
	switch(pixelFormat) {          
		case kCCTexture2DPixelFormat_RGBA8888:
		case kCCTexture2DPixelFormat_RGBA4444:
		case kCCTexture2DPixelFormat_RGB5A1:
		case kCCTexture2DPixelFormat_RGB565:
		case kCCTexture2DPixelFormat_A8:
			tempData = (unsigned char*)(this->getData());
			CCAssert(tempData != NULL, "NULL image data.");
			
			if(this->getWidth() == (short)POTWide && this->getHeight() == (short)POTHigh)
			{
				data = new unsigned char[POTHigh * POTWide * 4];
				memcpy(data, tempData, POTHigh * POTWide * 4);
			}
			else
			{
				data = new unsigned char[POTHigh * POTWide * 4];
				memset(data, 0, POTHigh * POTWide * 4);
				
				unsigned char* pPixelData = (unsigned char*) tempData;
				unsigned char* pTargetData = (unsigned char*) data;
				
				int imageHeight = this->getHeight();
				for(int y = 0; y < imageHeight; ++y)
				{
					memcpy(pTargetData+POTWide*4*y, pPixelData+(this->getWidth())*4*y, (this->getWidth())*4);
				}
			}
			
			break;    
		case kCCTexture2DPixelFormat_RGB888:
			tempData = (unsigned char*)(this->getData());
			CCAssert(tempData != NULL, "NULL image data.");
			if(this->getWidth() == (short)POTWide && this->getHeight() == (short)POTHigh)
			{
				data = new unsigned char[POTHigh * POTWide * 3];
				memcpy(data, tempData, POTHigh * POTWide * 3);
			}
			else
			{
				data = new unsigned char[POTHigh * POTWide * 3];
				memset(data, 0, POTHigh * POTWide * 3);
				
				unsigned char* pPixelData = (unsigned char*) tempData;
				unsigned char* pTargetData = (unsigned char*) data;
				
				int imageHeight = this->getHeight();
				for(int y = 0; y < imageHeight; ++y)
				{
					memcpy(pTargetData+POTWide*3*y, pPixelData+(this->getWidth())*3*y, (this->getWidth())*3);
				}
			}
			break;   
		default:
			CCAssert(0, "Invalid pixel format");
	}
	
	// Repack the pixel data into the right format
	
	if(pixelFormat == kCCTexture2DPixelFormat_RGB565) {
		//Convert "RRRRRRRRRGGGGGGGGBBBBBBBBAAAAAAAA" to "RRRRRGGGGGGBBBBB"
		tempData = new unsigned char[POTHigh * POTWide * 2];
		inPixel32 = (unsigned int*)data;
		outPixel16 = (unsigned short*)tempData;
		
		unsigned int length = POTWide * POTHigh;
		for(unsigned int i = 0; i < length; ++i, ++inPixel32)
		{
			*outPixel16++ = 
			((((*inPixel32 >> 0) & 0xFF) >> 3) << 11) |  // R
			((((*inPixel32 >> 8) & 0xFF) >> 2) << 5) |   // G
			((((*inPixel32 >> 16) & 0xFF) >> 3) << 0);   // B
		}
		
		delete [] data;
		data = tempData;
	}
	else if (pixelFormat == kCCTexture2DPixelFormat_RGBA4444) {
		//Convert "RRRRRRRRRGGGGGGGGBBBBBBBBAAAAAAAA" to "RRRRGGGGBBBBAAAA"
		tempData = new unsigned char[POTHigh * POTWide * 2];
		inPixel32 = (unsigned int*)data;
		outPixel16 = (unsigned short*)tempData;
		
		unsigned int length = POTWide * POTHigh;
		for(unsigned int i = 0; i < length; ++i, ++inPixel32)
		{
			*outPixel16++ = 
			((((*inPixel32 >> 0) & 0xFF) >> 4) << 12) | // R
			((((*inPixel32 >> 8) & 0xFF) >> 4) << 8) | // G
			((((*inPixel32 >> 16) & 0xFF) >> 4) << 4) | // B
			((((*inPixel32 >> 24) & 0xFF) >> 4) << 0); // A
		}
		
		delete [] data;
		data = tempData;
	}
	else if (pixelFormat == kCCTexture2DPixelFormat_RGB5A1) {
		//Convert "RRRRRRRRRGGGGGGGGBBBBBBBBAAAAAAAA" to "RRRRRGGGGGBBBBBA"
		tempData = new unsigned char[POTHigh * POTWide * 2];
		inPixel32 = (unsigned int*)data;
		outPixel16 = (unsigned short*)tempData;
		
		unsigned int length = POTWide * POTHigh;
		for(unsigned int i = 0; i < length; ++i, ++inPixel32)
		{
			*outPixel16++ = 
			((((*inPixel32 >> 0) & 0xFF) >> 3) << 11) | // R
			((((*inPixel32 >> 8) & 0xFF) >> 3) << 6) | // G
			((((*inPixel32 >> 16) & 0xFF) >> 3) << 1) | // B
			((((*inPixel32 >> 24) & 0xFF) >> 7) << 0); // A
		}
		
		delete []data;
		data = tempData;
	}
	else if (pixelFormat == kCCTexture2DPixelFormat_A8)
	{
		// fix me, how to convert to A8
		pixelFormat = kCCTexture2DPixelFormat_RGBA8888;
		
		//
		//The code can not work, how to convert to A8?
		//
		//tempData = new unsigned char[POTHigh * POTWide];
		//inPixel32 = (unsigned int*)data;
		//outPixel8 = tempData;
		 
		//unsigned int length = POTWide * POTHigh;
		//for(unsigned int i = 0; i < length; ++i, ++inPixel32)
		//{
		//    *outPixel8++ = (*inPixel32 >> 24) & 0xFF;
		//}
		 
		//delete []data;
		//data = tempData;
		 
	}
	
	if (data)
	{
		CC_SAFE_DELETE_ARRAY(m_pData);
		m_pData = data;
	}
	return true;	
}
*/

//bool CCImage::initWithImageData(void * pData, int nDataLen, EImageFormat eFmt/* = eSrcFmtPng*/)
bool CCImage::initWithImageData(void * pData, 
                           int nDataLen, 
                           EImageFormat eFmt,
                           int nWidth,
                           int nHeight,
                           int nBitsPerComponent)
{
    bool bRet = false;
    tImageInfo info = {0};
    do 
    {
        CC_BREAK_IF(! pData || nDataLen <= 0);
        
        if (eFmt == kFmtRawData)
        {
            bRet = _initWithRawData(pData, nDataLen, nWidth, nHeight, nBitsPerComponent, false);
        }
        else if (eFmt == kFmtWebp)
        {
            bRet = _initWithWebpData(pData, nDataLen);
        }
        else
        {
            bRet = _initWithData(pData, nDataLen, &info, 1.0f, 1.0f);//m_dScaleX, m_dScaleY);
            if (bRet)
            {
                m_nHeight = (short)info.height;
                m_nWidth = (short)info.width;
                m_nBitsPerComponent = info.bitsPerComponent;
                if (eFmt == kFmtJpg)
                {
                    m_bHasAlpha = true;
                    m_bPreMulti = false;
                }
                else
                {
                    m_bHasAlpha = info.hasAlpha;
                    m_bPreMulti = info.isPremultipliedAlpha;
                }
                m_pData = info.data;
            }
        }
    } while (0);
	
    return bRet;
}

bool CCImage::_initWithRawData(void *pData, int nDatalen, int nWidth, int nHeight, int nBitsPerComponent, bool bPreMulti)
{
    bool bRet = false;
    do
    {
        CC_BREAK_IF(0 == nWidth || 0 == nHeight);
        
        m_nBitsPerComponent = nBitsPerComponent;
        m_nHeight   = (short)nHeight;
        m_nWidth    = (short)nWidth;
        m_bHasAlpha = true;
        
        // only RGBA8888 supported
        int nBytesPerComponent = 4;
        int nSize = nHeight * nWidth * nBytesPerComponent;
        m_pData = new unsigned char[nSize];
        CC_BREAK_IF(! m_pData);
        memcpy(m_pData, pData, nSize);
        
        bRet = true;
    } while (0);
    return bRet;
}

bool CCImage::initWithString(
	const char *    pText, 
	int             nWidth, 
	int             nHeight,
	ETextAlign      eAlignMask,
	const char *    pFontName,
	int             nSize)
{
    tImageInfo info = {0};
    info.width = nWidth;
    info.height = nHeight;
	
    if (! _initWithString(pText, eAlignMask, pFontName, nSize, &info, NULL)) //pStrokeColor))
    {
        return false;
    }
    m_nHeight = (short)info.height;
    m_nWidth = (short)info.width;
    m_nBitsPerComponent = info.bitsPerComponent;
    m_bHasAlpha = info.hasAlpha;
    m_bPreMulti = info.isPremultipliedAlpha;
	if (m_pData) {
		CC_SAFE_DELETE_ARRAY(m_pData);
	}
    m_pData = info.data;

    return true;
}

bool CCImage::saveToFile(const char *pszFilePath, bool bIsToRGB)
{
	//assert(false);
	return false;
}

//
////将图像数据保存为图片文件，目前只支持PNG和JPG
////参1:文件路径
////参2:是否是RGB的像素格式
//bool CCImage::saveToFile(const char *pszFilePath, bool bIsToRGB)
//{
//    bool bRet = false;
//    
//    do
//    {
//        //参数有效性判断
//        CC_BREAK_IF(NULL == pszFilePath);
//        //通过是否有扩展名判断参数有效性。
//        std::string strFilePath(pszFilePath);
//        CC_BREAK_IF(strFilePath.size() <= 4);
//        //将路径名转为小写字符串
//        std::string strLowerCasePath(strFilePath);
//        for (unsigned int i = 0; i < strLowerCasePath.length(); ++i)
//        {
//            strLowerCasePath[i] = tolower(strFilePath[i]);
//        }
//        //通过扩展名转成相应的图片文件
//        //PNG
//        if (std::string::npos != strLowerCasePath.find(".png"))
//        {
//            CC_BREAK_IF(!_saveImageToPNG(pszFilePath, bIsToRGB));
//        }
//        //JPG
//        else if (std::string::npos != strLowerCasePath.find(".jpg"))
//        {
//            CC_BREAK_IF(!_saveImageToJPG(pszFilePath));
//        }
//        else
//        {
//            //不支持其它格式
//            break;
//        }
//        
//        bRet = true;
//    } while (0);
//    
//    return bRet;
//}
////将图像数据保存为PNG图片
//bool CCImage::_saveImageToPNG(const char * pszFilePath, bool bIsToRGB)
//{
//    bool bRet = false;
//    do
//    {
//        //参数有效性判断
//        CC_BREAK_IF(NULL == pszFilePath);
//        //使用libpng来写PNG文件。
//        //定义文件指针变量用于写文件
//        FILE *fp;
//        //定义libpng所用的一些信息结构
//        png_structp png_ptr;
//        png_infop info_ptr;
//        png_colorp palette;
//        png_bytep *row_pointers;
//        //打开文件开始写入
//        fp = fopen(pszFilePath, "wb");
//        CC_BREAK_IF(NULL == fp);
//        //创建写PNG的文件结构体,将其结构指针返回给png_ptr
//        png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
//        //指针有效性判断
//        if (NULL == png_ptr)
//        {
//            fclose(fp);
//            break;
//        }
//        //创建PNG的信息结构体，将其结构指针返回给info_ptr。
//        info_ptr = png_create_info_struct(png_ptr);
//        if (NULL == info_ptr)
//        {
//            fclose(fp);
//            png_destroy_write_struct(&png_ptr, NULL);
//            break;
//        }
//#if (CC_TARGET_PLATFORM != CC_PLATFORM_BADA)
//        if (setjmp(png_jmpbuf(png_ptr)))
//        {
//            fclose(fp);
//            png_destroy_write_struct(&png_ptr, &info_ptr);
//            break;
//        }
//#endif
//        //初始化png_ptr
//        png_init_io(png_ptr, fp);
//        //根据是否有ALPHA来写入相应的头信息
//        if (!bIsToRGB && m_bHasAlpha)
//        {
//            png_set_IHDR(png_ptr, info_ptr, m_nWidth, m_nHeight, 8, PNG_COLOR_TYPE_RGB_ALPHA,
//                         PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
//        }
//        else
//        {
//            png_set_IHDR(png_ptr, info_ptr, m_nWidth, m_nHeight, 8, PNG_COLOR_TYPE_RGB,
//                         PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
//        }
//        //创建调色板
//        palette = (png_colorp)png_malloc(png_ptr, PNG_MAX_PALETTE_LENGTH * sizeof (png_color));
//        //设置调色板
//        png_set_PLTE(png_ptr, info_ptr, palette, PNG_MAX_PALETTE_LENGTH);
//        //写入info_ptr
//        png_write_info(png_ptr, info_ptr);
//        //
//        png_set_packing(png_ptr);
//        //申请临时存储m_pData中每一行像素数据地址的内存空间，将申请到的内存地址返回给row_pointers。
//        row_pointers = (png_bytep *)malloc(m_nHeight * sizeof(png_bytep));
//        if(row_pointers == NULL)
//        {
//            fclose(fp);
//            png_destroy_write_struct(&png_ptr, &info_ptr);
//            break;
//        }
//        //根据是否有ALPHA分别处理写入像素数据到文件中。
//        if (!m_bHasAlpha)
//        {
//            //如果没有ALPHA，只是RGB，这里将m_pData中数据，遍历每一行，将每一行的起始内存地址放入row_pointers指针数组中。
//            for (int i = 0; i < (int)m_nHeight; i++)
//            {
//                row_pointers[i] = (png_bytep)m_pData + i * m_nWidth * 3;
//            }
//            //将row_pointers中指向的每一行数据写入文件。
//            png_write_image(png_ptr, row_pointers);
//            
//            //释放内存
//            free(row_pointers);
//            row_pointers = NULL;
//        }
//        else
//        {
//            //如果带ALPHA通道。对是否是RGB格式又进行分别处理。
//            //如果是RGB888格式
//            if (bIsToRGB)
//            {
//                //创建临时的内存存放像素数据。每个像素3字节，分别存R,G,B值。
//                unsigned char *pTempData = new unsigned char[m_nWidth * m_nHeight * 3];
//                if (NULL == pTempData)
//                {
//                    fclose(fp);
//                    png_destroy_write_struct(&png_ptr, &info_ptr);
//                    break;
//                }
//                //双循环遍历每个像素，将R,G,B值保存到数组中。
//                for (int i = 0; i < m_nHeight; ++i)
//                {
//                    for (int j = 0; j < m_nWidth; ++j)
//                    {
//                        pTempData[(i * m_nWidth + j) * 3] = m_pData[(i * m_nWidth + j) * 4];
//                        pTempData[(i * m_nWidth + j) * 3 + 1] = m_pData[(i * m_nWidth + j) * 4 + 1];
//                        pTempData[(i * m_nWidth + j) * 3 + 2] = m_pData[(i * m_nWidth + j) * 4 + 2];
//                    }
//                }
//                //将数组中保存的每行像素的内存地址存入row_pointers数组中。
//                for (int i = 0; i < (int)m_nHeight; i++)
//                {
//                    row_pointers[i] = (png_bytep)pTempData + i * m_nWidth * 3;
//                }
//                //将row_pointers中指向的每一行数据写入文件。
//                png_write_image(png_ptr, row_pointers);
//                //释放内存
//                free(row_pointers);
//                row_pointers = NULL;
//                
//                CC_SAFE_DELETE_ARRAY(pTempData);
//            }
//            else
//            {
//                //如果是RGBA8888格式
//                //将数组中保存的每行像素的内存地址存入row_pointers数组中。
//                for (int i = 0; i < (int)m_nHeight; i++)
//                {
//                    row_pointers[i] = (png_bytep)m_pData + i * m_nWidth * 4;
//                }
//                //将row_pointers中指向的每一行数据写入文件。
//                png_write_image(png_ptr, row_pointers);
//                //释放内存
//                
//                free(row_pointers);
//                row_pointers = NULL;
//            }
//        }
//        //结束写PNG文件
//        png_write_end(png_ptr, info_ptr);
//        //释放相应的信息结构
//        png_free(png_ptr, palette);
//        palette = NULL;
//        png_destroy_write_struct(&png_ptr, &info_ptr);
//        
//        fclose(fp);
//        
//        bRet = true;
//    } while (0);
//    return bRet;
//}
//
////将图像数据保存为JPG文件
//bool CCImage::_saveImageToJPG(const char * pszFilePath)
//{
//    bool bRet = false;
//    do
//    {
//        //参数有效性判断
//        CC_BREAK_IF(NULL == pszFilePath);
//        //使用libjpg库要用到的相关结构。
//        struct jpeg_compress_struct cinfo;
//        struct jpeg_error_mgr jerr;
//        FILE * outfile;                 /* target file */
//        JSAMPROW row_pointer[1];        /* pointer to JSAMPLE row[s] */
//        int     row_stride;          /* physical row width in image buffer */
//        //初始化相关结构
//        cinfo.err = jpeg_std_error(&jerr);
//        jpeg_create_compress(&cinfo);
//        //开始写入文件
//        CC_BREAK_IF((outfile = fopen(pszFilePath, "wb")) == NULL);
//        //写入JPG头文件基本信息
//        jpeg_stdio_dest(&cinfo, outfile);
//        //填充JPG图像的属性信息结构
//        cinfo.image_width = m_nWidth;
//        cinfo.image_height = m_nHeight;
//        cinfo.input_components = 3;
//        cinfo.in_color_space = JCS_RGB;
//        //将信息结构来设置JPG图像
//        jpeg_set_defaults(&cinfo);
//        //开始进行数据压缩输出
//        jpeg_start_compress(&cinfo, TRUE);
//        //设置每行的字节长度
//        row_stride = m_nWidth * 3;
//        //跟据图像数据是否有ALPHA通道来进行分别处理
//        if (m_bHasAlpha)
//        {
//            //创建内存来存放图像的像素数据。
//            unsigned char *pTempData = new unsigned char[m_nWidth * m_nHeight * 3];
//            if (NULL == pTempData)
//            {
//                jpeg_finish_compress(&cinfo);
//                jpeg_destroy_compress(&cinfo);
//                fclose(outfile);
//                break;
//            }
//            //双循环遍历每一个图像像素的数据，取R,G,B值存放到临时申请的内存地址中,A值弃之不取。
//            for (int i = 0; i < m_nHeight; ++i)
//            {
//                for (int j = 0; j < m_nWidth; ++j)
//                    
//                {
//                    //因图像数据有A通道，所以找相应的像素地址时会由像素索引x4。
//                    pTempData[(i * m_nWidth + j) * 3] = m_pData[(i * m_nWidth + j) * 4];
//                    pTempData[(i * m_nWidth + j) * 3 + 1] = m_pData[(i * m_nWidth + j) * 4 + 1];
//                    pTempData[(i * m_nWidth + j) * 3 + 2] = m_pData[(i * m_nWidth + j) * 4 + 2];
//                }
//            }
//            //将扫描行的数据写入JPG文件
//            while (cinfo.next_scanline < cinfo.image_height) {
//                row_pointer[0] = & pTempData[cinfo.next_scanline * row_stride];
//                (void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
//            }
//            
//            CC_SAFE_DELETE_ARRAY(pTempData);
//        }
//        else
//        { //将扫描行的数据写入JPG文件
//            while (cinfo.next_scanline < cinfo.image_height) {
//                row_pointer[0] = & m_pData[cinfo.next_scanline * row_stride];
//                (void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
//            }
//        }
//        //结束数据压缩，关闭文件并释放相应信息结构。
//        jpeg_finish_compress(&cinfo);
//        fclose(outfile);
//        jpeg_destroy_compress(&cinfo);
//        
//        bRet = true;
//    } while (0);
//    return bRet;
//}
//
//NS_CC_END
//
//现在我们回到CCImage.cpp:
//
//[cpp]
//#define __CC_PLATFORM_IMAGE_CPP__
//#include "platform/CCImageCommon_cpp.h"
//
//NS_CC_BEGIN
//
////此处定义一个BitmapDC类在位图上进行文字绘制。
//class BitmapDC
//{
//public:
//    //构造函数
//    BitmapDC(HWND hWnd = NULL)
//    : m_hDC(NULL)
//    , m_hBmp(NULL)
//    , m_hFont((HFONT)GetStockObject(DEFAULT_GUI_FONT))
//    , m_hWnd(NULL)
//    {
//        //保存窗口句柄
//        m_hWnd = hWnd;
//        //取得窗口的hdc
//        HDC hdc = GetDC(hWnd);
//        //创建兼容的hdc
//        m_hDC   = CreateCompatibleDC(hdc);
//        //释放hdc
//        ReleaseDC(hWnd, hdc);
//    }
//    //析构函数
//    ~BitmapDC()
//    {
//        prepareBitmap(0, 0);
//        if (m_hDC)
//        {
//            DeleteDC(m_hDC);
//        }
//        //创建字体
//        HFONT hDefFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
//        if (hDefFont != m_hFont)
//        {
//            DeleteObject(m_hFont);
//            m_hFont = hDefFont;
//        }
//        // release temp font resource
//        if (m_curFontPath.size() > 0)
//        {
//            wchar_t * pwszBuffer = utf8ToUtf16(m_curFontPath);
//            if (pwszBuffer)
//            {
//                RemoveFontResource(pwszBuffer);
//                SendMessage( m_hWnd, WM_FONTCHANGE, 0, 0);
//                delete [] pwszBuffer;
//                pwszBuffer = NULL;
//            }
//        }
//    }
//    //多字节转宽字符
//    wchar_t * utf8ToUtf16(std::string nString)
//    {
//        wchar_t * pwszBuffer = NULL;
//        do
//        {
//            if (nString.size() < 0)
//            {
//                break;
//            }
//            // utf-8 to utf-16
//            int nLen = nString.size();
//            int nBufLen  = nLen + 1;
//            pwszBuffer = new wchar_t[nBufLen];
//            CC_BREAK_IF(! pwszBuffer);
//            memset(pwszBuffer,0,nBufLen);
//            nLen = MultiByteToWideChar(CP_UTF8, 0, nString.c_str(), nLen, pwszBuffer, nBufLen);
//            pwszBuffer[nLen] = '\0';
//        } while (0);
//        return pwszBuffer;
//        
//    }
//    //设置使用的字体和大小
//    bool setFont(const char * pFontName = NULL, int nSize = 0)
//    {
//        bool bRet = false;
//        do
//        {
//            //创建字体
//            std::string fontName = pFontName;
//            std::string fontPath;
//            HFONT       hDefFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
//            LOGFONTA    tNewFont = {0};
//            LOGFONTA    tOldFont = {0};
//            GetObjectA(hDefFont, sizeof(tNewFont), &tNewFont);
//            if (fontName.c_str())
//            {
//                // 如果字体名称是ttf文件，取得其全路径名
//                int nFindttf = fontName.find(".ttf");
//                int nFindTTF = fontName.find(".TTF");
//                if (nFindttf >= 0 || nFindTTF >= 0)
//                {
//                    fontPath = CCFileUtils::sharedFileUtils()->fullPathFromRelativePath(fontName.c_str());
//                    int nFindPos = fontName.rfind("/");
//                    fontName = &fontName[nFindPos+1];
//                    nFindPos = fontName.rfind(".");
//                    fontName = fontName.substr(0,nFindPos);
//                }
//                tNewFont.lfCharSet = DEFAULT_CHARSET;
//                strcpy_s(tNewFont.lfFaceName, LF_FACESIZE, fontName.c_str());
//            }
//            if (nSize)
//            {
//                tNewFont.lfHeight = -nSize;
//            }
//            //
//            GetObjectA(m_hFont,  sizeof(tOldFont), &tOldFont);
//            
//            if (tOldFont.lfHeight == tNewFont.lfHeight
//                && ! strcpy(tOldFont.lfFaceName, tNewFont.lfFaceName))
//            {
//                // already has the font
//                bRet = true;
//                break;
//            }
//            
//            // 删除旧的字体
//            if (m_hFont != hDefFont)
//            {
//                DeleteObject(m_hFont);
//                // release old font register
//                if (m_curFontPath.size() > 0)
//                {
//                    wchar_t * pwszBuffer = utf8ToUtf16(m_curFontPath);
//                    if (pwszBuffer)
//                    {
//                        if(RemoveFontResource(pwszBuffer))
//                        {
//                            SendMessage( m_hWnd, WM_FONTCHANGE, 0, 0);
//                        }
//                        delete [] pwszBuffer;
//                        pwszBuffer = NULL;
//                    }
//                }
//                fontPath.size()>0?(m_curFontPath = fontPath):(m_curFontPath.clear());
//                
//                if (m_curFontPath.size() > 0)
//                {
//                    wchar_t * pwszBuffer = utf8ToUtf16(m_curFontPath);
//                    if (pwszBuffer)
//                    {
//                        if(AddFontResource(pwszBuffer))
//                        {
//                            SendMessage( m_hWnd, WM_FONTCHANGE, 0, 0);
//                        }
//                        delete [] pwszBuffer;
//                        pwszBuffer = NULL;
//                    }
//                }
//            }
//            m_hFont = NULL;
//            
//            // 字体不使用锐利字体
//            tNewFont.lfQuality = ANTIALIASED_QUALITY;
//            
//            // 创建新字体
//            m_hFont = CreateFontIndirectA(&tNewFont);
//            if (! m_hFont)
//            {
//                // create failed, use default font
//                m_hFont = hDefFont;
//                break;
//            }
//            
//            bRet = true;
//        } while (0);
//        return bRet;
//    }
//    //取得使用当前字体写出的字符串所占据的图像大小。
//    SIZE sizeWithText(const wchar_t * pszText, int nLen, DWORD dwFmt, LONG nWidthLimit)
//    {
//        SIZE tRet = {0};
//        do
//        {
//            CC_BREAK_IF(! pszText || nLen <= 0);
//            
//            RECT rc = {0, 0, 0, 0};
//            DWORD dwCalcFmt = DT_CALCRECT;
//            
//            if (nWidthLimit > 0)
//            {
//                rc.right = nWidthLimit;
//                dwCalcFmt |= DT_WORDBREAK
//                | (dwFmt & DT_CENTER)
//                | (dwFmt & DT_RIGHT);
//            }
//            // 使用当前字体。
//            HGDIOBJ hOld = SelectObject(m_hDC, m_hFont);
//            
//            // 写字。
//            DrawTextW(m_hDC, pszText, nLen, &rc, dwCalcFmt);
//            SelectObject(m_hDC, hOld);
//            
//            tRet.cx = rc.right;
//            tRet.cy = rc.bottom;
//        } while (0);
//        
//        return tRet;
//    }
//    //创建一个指定大小的位图。
//    bool prepareBitmap(int nWidth, int nHeight)
//    {
//        // 释放原来的位图
//        if (m_hBmp)
//        {
//            DeleteObject(m_hBmp);
//            m_hBmp = NULL;
//        }       //如果大小有效则创建位图。
//        if (nWidth > 0 && nHeight > 0)
//        {
//            m_hBmp = CreateBitmap(nWidth, nHeight, 1, 32, NULL);
//            if (! m_hBmp)
//            {
//                return false;
//            }
//        }
//        return true;
//    }
//    //在指定位置按照指定对齐方式写字。
//    //参1:字符串
//    //参2:指定拉置及大小
//    //参3:文字对齐方式
//    int drawText(const char * pszText, SIZE& tSize, CCImage::ETextAlign eAlign)
//    {
//        int nRet = 0;
//        wchar_t * pwszBuffer = 0;
//        do
//        {
//            CC_BREAK_IF(! pszText);
//            
//            DWORD dwFmt = DT_WORDBREAK;
//            DWORD dwHoriFlag = eAlign & 0x0f;
//            DWORD dwVertFlag = (eAlign & 0xf0) >> 4;
//            //设置横向对齐方式。
//            switch (dwHoriFlag)
//            {
//                case 1: // 左对齐
//                    dwFmt |= DT_LEFT;
//                    break;
//                case 2: // 右对齐
//                    dwFmt |= DT_RIGHT;
//                    break;
//                case 3: // 居中
//                    dwFmt |= DT_CENTER;
//                    break;
//            }
//            //取得字符串的字节长度。
//            int nLen = strlen(pszText);
//            int nBufLen  = nLen + 1;
//            // 为转换后的宽字符串申请内存地址。
//            pwszBuffer = new wchar_t[nBufLen];
//            CC_BREAK_IF(! pwszBuffer);
//            //内存数据置零
//            memset(pwszBuffer, 0, sizeof(wchar_t)*nBufLen);
//            // 将多字节转宽字符串。
//            nLen = MultiByteToWideChar(CP_UTF8, 0, pszText, nLen, pwszBuffer, nBufLen);
//            //取得写字符串占据的图像区域大小
//            SIZE newSize = sizeWithText(pwszBuffer, nLen, dwFmt, tSize.cx);
//            //建立RECT变量做为实际绘制占据的图像区域大小
//            RECT rcText = {0};
//            // 如果宽度为0，则使用显示字符串所需的图像大小。
//            if (tSize.cx <= 0)
//            {
//                tSize = newSize;
//                rcText.right  = newSize.cx;
//                rcText.bottom = newSize.cy;
//            }
//            else
//            {
//                LONG offsetX = 0;
//                LONG offsetY = 0;
//                rcText.right = newSize.cx;
//                //根据对齐方式计算横向偏移。
//                if (1 != dwHoriFlag
//                    && newSize.cx < tSize.cx)
//                {
//                    offsetX = (2 == dwHoriFlag) ? tSize.cx - newSize.cx                                 : (tSize.cx - newSize.cx) / 2;                                          }
//                //如果指定矩形高度为0，则使用显示字符串所需的图像高度。
//                if (tSize.cy <= 0)
//                {
//                    tSize.cy = newSize.cy;
//                    dwFmt   |= DT_NOCLIP;
//                    rcText.bottom = newSize.cy; // store the text height to rectangle
//                }
//                else if (tSize.cy < newSize.cy)
//                {
//                    // content height larger than text height need, clip text to rect
//                    rcText.bottom = tSize.cy;
//                }
//                else
//                {
//                    rcText.bottom = newSize.cy;
//                    
//                    // content larger than text, need adjust vertical position
//                    dwFmt |= DT_NOCLIP;
//                    
//                    //根据对齐方式计算纵向偏移。
//                    offsetY = (2 == dwVertFlag) ? tSize.cy - newSize.cy     // 居下                        : (3 == dwVertFlag) ? (tSize.cy - newSize.cy) / 2   // 居中                        : 0;                                                // 居上                }
//                    //如果需要,调整偏移。
//                    if (offsetX || offsetY)
//                    {
//                        OffsetRect(&rcText, offsetX, offsetY);
//                    }
//                }
//                //创建相应大小的位图。
//                CC_BREAK_IF(! prepareBitmap(tSize.cx, tSize.cy));
//                // 使用当前字体和位图
//                HGDIOBJ hOldFont = SelectObject(m_hDC, m_hFont);
//                HGDIOBJ hOldBmp  = SelectObject(m_hDC, m_hBmp);
//                //设置背景透明模式和写字的颜色
//                SetBkMode(m_hDC, TRANSPARENT);
//                SetTextColor(m_hDC, RGB(255, 255, 255)); // white color
//                //写字
//                nRet = DrawTextW(m_hDC, pwszBuffer, nLen, &rcText, dwFmt);
//                //DrawTextA(m_hDC, pszText, nLen, &rcText, dwFmt);
//                //还原为之前使用的字体和位图
//                SelectObject(m_hDC, hOldBmp);
//                SelectObject(m_hDC, hOldFont);
//            } while (0);
//            //释放内存
//            CC_SAFE_DELETE_ARRAY(pwszBuffer);
//            return nRet;
//        }
//        //成员变量m_hDC及get接口
//        CC_SYNTHESIZE_READONLY(HDC, m_hDC, DC);
//        //成员变量m_hBmp及get接口
//        CC_SYNTHESIZE_READONLY(HBITMAP, m_hBmp, Bitmap);
//    private:
//        //友元类CCImage
//        friend class CCImage;
//        //成员变量m_hFont代表字体
//        HFONT   m_hFont;
//        //成员变量m_hWnd代表当前窗口句柄。
//        HWND    m_hWnd;
//        //成员m_curFontPath代表当前字体ttf文件全路径。
//        std::string m_curFontPath;
//    };
//    //取得单例BitmapDC
//    static BitmapDC& sharedBitmapDC()
//    {
//        static BitmapDC s_BmpDC;
//        return s_BmpDC;
//    }
//    //CCImage的成员函数，使用相应的字体写字符串生成图像数据。
//    //参1:字符串
//    //参2:要创建的图片宽度，如果填0，则按照字符串的宽度进行设置。
//    //参3:要创建的图片高度，如果填0，则按照字符串的高度进行设置。
//    //参4:文字的对齐方式。
//    //参5:字体名称
//    //参6:字体大小
//    bool CCImage::initWithString(
//                                 const char *    pText,
//                                 int             nWidth/* = 0*/,
//                                 int             nHeight/* = 0*/,
//                                 ETextAlign      eAlignMask/* = kAlignCenter*/,
//                                 const char *    pFontName/* = nil*/,
//                                 int             nSize/* = 0*/)
//    {
//        bool bRet = false;
//        unsigned char * pImageData = 0;
//        do
//        {
//            CC_BREAK_IF(! pText);
//            //取得单例BitmapDC
//            BitmapDC& dc = sharedBitmapDC();
//            //设置使用的字体和大小
//            if (! dc.setFont(pFontName, nSize))
//            {
//                CCLog("Can't found font(%s), use system default", pFontName);
//            }
//            
//            // 写字
//            SIZE size = {nWidth, nHeight};
//            CC_BREAK_IF(! dc.drawText(pText, size, eAlignMask));
//            
//            //申请图像大小的内存，成功申请后将其地址返回给指针pImageData。
//            pImageData = new unsigned char[size.cx * size.cy * 4];
//            CC_BREAK_IF(! pImageData);
//            //创建位图头信息结构
//            struct
//            {
//                BITMAPINFOHEADER bmiHeader;
//                int mask[4];
//            } bi = {0};
//            bi.bmiHeader.biSize = sizeof(bi.bmiHeader);
//            //取得写字符串的位图像素
//            CC_BREAK_IF(! GetDIBits(dc.getDC(), dc.getBitmap(), 0, 0,
//                                    NULL, (LPBITMAPINFO)&bi, DIB_RGB_COLORS));
//            
//            m_nWidth    = (short)size.cx;
//            m_nHeight   = (short)size.cy;
//            m_bHasAlpha = true;
//            m_bPreMulti = false;
//            m_pData     = pImageData;
//            pImageData  = 0;
//            m_nBitsPerComponent = 8;
//            // 位图像素拷到pImageData中。
//            bi.bmiHeader.biHeight = (bi.bmiHeader.biHeight > 0)
//            ? - bi.bmiHeader.biHeight : bi.bmiHeader.biHeight;
//            GetDIBits(dc.getDC(), dc.getBitmap(), 0, m_nHeight, m_pData,  
//                      (LPBITMAPINFO)&bi, DIB_RGB_COLORS); 
//            
//            // 双循环遍历每个像素。取得R，G，B值组成4字节的RGBA值保存。 
//            COLORREF * pPixel = NULL; 
//            for (int y = 0; y < m_nHeight; ++y) 
//            { 
//                pPixel = (COLORREF *)m_pData + y * m_nWidth; 
//                for (int x = 0; x < m_nWidth; ++x) 
//                { 
//                    COLORREF& clr = *pPixel; 
//                    if (GetRValue(clr) || GetGValue(clr) || GetBValue(clr)) 
//                    { 
//                        clr |= 0xff000000; 
//                    } 
//                    ++pPixel; 
//                } 
//            } 
//            
//            bRet = true; 
//        } while (0); 
//        
//        return bRet; 
//    } 
//    
//

NS_CC_END

